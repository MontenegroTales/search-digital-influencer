import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DigitalInfluencerComponent } from './digital-influencer/digital-influencer.component';
import { InputsFormComponent } from './inputs-form/inputs-form.component';

const routes: Routes = [
  { path: '', component: InputsFormComponent },
  { path: 'digital-influencer', component: DigitalInfluencerComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule] 
})
export class AppRoutingModule { }
