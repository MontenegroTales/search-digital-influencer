import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DigitalInfluencerComponent } from './digital-influencer.component';

describe('DigitalInfluencerComponent', () => {
  let component: DigitalInfluencerComponent;
  let fixture: ComponentFixture<DigitalInfluencerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DigitalInfluencerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DigitalInfluencerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
