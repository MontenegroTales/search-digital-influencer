import { 
    MatFormFieldModule, 
    MatIconModule, 
    MatInputModule, 
    MatCardModule, 
    MatGridListModule,
    MatButtonModule
} from '@angular/material';
import { NgModule } from '@angular/core';


@NgModule({
    imports: [
        MatFormFieldModule, 
        MatIconModule,
        MatInputModule, 
        MatCardModule, 
        MatGridListModule,
        MatButtonModule
    ],
    exports: [
        MatFormFieldModule, 
        MatIconModule,
        MatInputModule, 
        MatCardModule, 
        MatGridListModule,
        MatButtonModule
    ]
})

export class MaterialModule { }