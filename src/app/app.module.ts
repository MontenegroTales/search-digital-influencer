import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material';

import { EmailInputComponent } from './inputs-form/email-input/email-input.component';
import { SearchInputComponent } from './inputs-form/search-input/search-input.component';
import { DigitalInfluencerComponent } from './digital-influencer/digital-influencer.component';
import { SearchBtnComponent } from './inputs-form/search-btn/search-btn.component';
import { InputsFormComponent } from './inputs-form/inputs-form.component';

@NgModule({
  declarations: [
    AppComponent,
    EmailInputComponent,
    SearchInputComponent,
    DigitalInfluencerComponent,
    SearchBtnComponent,
    InputsFormComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
